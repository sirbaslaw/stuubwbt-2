<?php $__env->startSection('pagetitle'); ?>
    EXAMINATION RESOURCES MANAGER - <small>Exam Instructions</small>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('maincontent'); ?>
    <div class="row margin-bottom-10">
        <div class="col-md-3 pull-right">
            <a href="<?php echo e(url('admin/wbt-manager')); ?>" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-aqua btn-u-xs"> Back to WBT Manager</a>
        </div>
    </div>
<div class="tab-v1">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#home" data-toggle="tab">Examination Instructions</a></li>
        <li><a href="#add-category" data-toggle="tab">Add New Instruction</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="home">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                         <thead>
                             <tr>
                                 <th>#</th>
                                 <th>Instruction Tile</th>
                                 <th>Description</th>
                             </tr>
                         </thead>
                         <tbody>
                         <?php /**/$count = 1/**/ ?>
                            <?php foreach($instructions as $instruction): ?>
                                <tr>
                                    <td><?php echo e($count); ?></td>
                                    <td><a href="#"><?php echo e($instruction->title); ?></a></td>
                                    <td><?php echo implode(' ', array_slice(explode(' ', $instruction->description), 0, 20)); ?></td>
                                </tr>
                                <?php /**/$count++/**/ ?>
                            <?php endforeach; ?>
                         </tbody>
                     </table> 
                </div>
            </div>
        </div>
        <div class="tab-pane fade in" id="add-category">
            <div class="row">
                <div class="col-md-12">
                    <?php echo Form::open(array('url' => url('admin/add-instruction'),'class'=>'sky-form', 'id'=>'sky-form')); ?>

                    <fieldset>
                        <div class="row">
                            <section class="col-12">
                                <label class="input">
                                    <span>INSTRUCTION TITLE</span>
                                    <input type="text" name="title" placeholder="Instruction Title" required>
                                </label>
                            </section>
                            <section class="col-12">
                                <label class="textarea">
                                    <span>INSTRUCTION DESCRIPTION</span>
                                    <textarea rows="3" name="description" id="texteditor"></textarea>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <footer>
                        <div class="pull-right">
                            <button type="submit" class="btn-u">Save</button>
                        </div>
                    </footer>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagejs'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('pageplugins'); ?>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/ckeditor/ckeditor.js')); ?>"></script>
    <script type="text/javascript">
    CKEDITOR.replace('texteditor');
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>