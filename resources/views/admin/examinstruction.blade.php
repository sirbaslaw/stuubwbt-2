@extends('admin_layout')

@section('pagetitle')
    EXAMINATION RESOURCES MANAGER - <small>Exam Instructions</small>
@stop
@section('maincontent')
    <div class="row margin-bottom-10">
        <div class="col-md-3 pull-right">
            <a href="{{url('admin/wbt-manager')}}" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-aqua btn-u-xs"> Back to WBT Manager</a>
        </div>
    </div>
<div class="tab-v1">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#home" data-toggle="tab">Examination Instructions</a></li>
        <li><a href="#add-category" data-toggle="tab">Add New Instruction</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="home">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                         <thead>
                             <tr>
                                 <th>#</th>
                                 <th>Instruction Tile</th>
                                 <th>Description</th>
                             </tr>
                         </thead>
                         <tbody>
                         {{--*/$count = 1/*--}}
                            @foreach($instructions as $instruction)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td><a href="#">{{$instruction->title}}</a></td>
                                    <td>{!! implode(' ', array_slice(explode(' ', $instruction->description), 0, 20)) !!}</td>
                                </tr>
                                {{--*/$count++/*--}}
                            @endforeach
                         </tbody>
                     </table> 
                </div>
            </div>
        </div>
        <div class="tab-pane fade in" id="add-category">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(array('url' => url('admin/add-instruction'),'class'=>'sky-form', 'id'=>'sky-form')) !!}
                    <fieldset>
                        <div class="row">
                            <section class="col-12">
                                <label class="input">
                                    <span>INSTRUCTION TITLE</span>
                                    <input type="text" name="title" placeholder="Instruction Title" required>
                                </label>
                            </section>
                            <section class="col-12">
                                <label class="textarea">
                                    <span>INSTRUCTION DESCRIPTION</span>
                                    <textarea rows="3" name="description" id="texteditor"></textarea>
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <footer>
                        <div class="pull-right">
                            <button type="submit" class="btn-u">Save</button>
                        </div>
                    </footer>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('pagejs')

@stop
@section('pageplugins')
    <script type="text/javascript" src="{{ asset('public/assets/plugins/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript">
    CKEDITOR.replace('texteditor');
    </script>
@stop