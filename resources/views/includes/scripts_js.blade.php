<!-- JS Global Compulsory -->
<script type="text/javascript" src="{{ asset('public/assets/plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/plugins/jquery/jquery-migrate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/plugins/jquery.countdown.js')}}"></script>

<!-- JS Implementing Plugins -->
<script type="text/javascript" src="{{ asset('public/assets/plugins/back-to-top.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/plugins/smoothScroll.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/plugins/modernizr.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/plugins/login-signup-modal-window/js/main.js')}}"></script>

<!-- JS Page Level -->
@yield('pagejs')

<!-- JS Google Analytics Tracking -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-84695501-1', 'auto');
    ga('send', 'pageview');

</script>